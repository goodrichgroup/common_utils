import matplotlib.pyplot as plt
import matplotlib
from matplotlib.collections import PatchCollection, LineCollection
import numpy as np


def draw_sphere_packing(positions, diameters, box_size,
                        contacts=None, draw_periodic_boundary=True, 
                        cdata=None, cmin=None, cmax=None,
                        cmap='plasma', ax=None, figsize=5):
    """Draw a packing of spherical particles with arbitrary diameters and colors in free or periodic boundary conditions.

    Args:
        positions (np.ndarray): Particle positions, shape = (n, d).
        diameters (np.ndarray): Particle diameters, shape = (n,).
        box_size (float): Size of the simulation box.
        contacts (np.ndarray, optional): List of particle contacts, shape = (n_bonds, 2). Used for drawing the bond network. Defaults to None.
        draw_periodic_boundary (bool, optional): If True, boundary particles will be copied to display periodic boundaries correctly. Defaults to True.
        cdata (np.ndarray, optional): Particle color data, shape = (n,). Defaults to None.
        cmin (float, optional): Lower cutoff of the color data. Defaults to None.
        cmax (float, optional): Upper cutoff of the color data. Defaults to None.
        cmap (str, optional): Name of the matplotlib colormap to use when displaying species types. Only used if colors is set. Defaults to 'plasma'.
        ax (matplotlib.axes.Axes, optional): Matplotlib Axes object used to embed the plot. A new figure is generated if set to None. Defaults to None.
        figsize (int, optional): Size (in inches) of the generated figure. Only active if ax=None. Defaults to 5.

    Returns:
        None: Returns nothing. Generate a figure manually and use ax != None to process the plot further.
    """

    if ax is None:
        _, ax = plt.subplots(constrained_layout=True,
                             figsize=(figsize, figsize))

    if draw_periodic_boundary:
        # Copy the particles at the edge of the frame so
        # that periodic boundaries are drawn correctly
        extra_idxs, shifts = _find_boundary_particles(
            positions, diameters, box_size)

        extra_positions = np.concatenate(
            [positions[idxs, :] + shift for idxs, shift in zip(extra_idxs, shifts)])
        positions = np.concatenate([positions, extra_positions])

        extra_idxs = np.concatenate(extra_idxs)
        diameters = np.concatenate([diameters, diameters[extra_idxs]])
        if cdata is not None:
            cdata = np.concatenate([cdata, cdata[extra_idxs]])

    circs = []
    for x, d in zip(positions, diameters):
        circ = plt.Circle(x, radius=d/2)
        circs.append(circ)
    circs = PatchCollection(circs, edgecolor='k', alpha=1)

    if cdata is not None:
        cmap = matplotlib.colormaps[cmap]
        circs.set_cmap(cmap)

        if cmin is None:
            cmin = np.min(cdata)
        if cmax is None:
            cmax = np.max(cdata)
            
        cdata = np.clip((cdata - cmin) / (cmax - cmin), 0, 1)
        circs.set_array(cdata)

    ax.add_collection(circs)

    if contacts is not None:
        dists = np.linalg.norm(
            positions[contacts[:, 0], :] - positions[contacts[:, 1], :], axis=1)
        shifts = np.vstack([np.zeros(2), _get_shifts(box_size)])

        lines = []
        for bond, dist in zip(contacts, dists):
            if dist < box_size / 2:
                lines += [positions[bond, :]]
            else:
                p1, p2 = bond
                p1s = positions[p1, :][None, :] + shifts
                p2s = positions[p2, :][None, :] + shifts
                d12 = np.linalg.norm(p1s[:, None] - p2s, axis=-1)

                idxs1, idxs2 = np.nonzero(d12 < box_size / 2)
                for i1, i2 in zip(idxs1, idxs2):
                    lines += [np.stack([p1s[i1], p2s[i2]])]

        lc = LineCollection(lines, color='k')

        ax.add_collection(lc)
        ax.scatter(positions[:, 0], positions[:, 1], s=4 *
                   figsize, ec='k', color='grey', zorder=20)

    ax.set_aspect('equal')
    ax.set_xlim(0, box_size)
    ax.set_ylim(0, box_size)
    ax.set_axis_off()
    return


def _find_boundary_particles(positions, diameters, box_size):
    max_diam = np.max(diameters)

    idxs_left = np.nonzero(positions[:, 0] < max_diam)[0]
    idxs_right = np.nonzero(positions[:, 0] > (box_size - max_diam))[0]
    idxs_bottom = np.nonzero(positions[:, 1] < max_diam)[0]
    idxs_top = np.nonzero(positions[:, 1] > (box_size - max_diam))[0]

    idxs_botleft = np.nonzero(positions[:, :] < max_diam)[0]
    idxs_botright = np.nonzero(
        (positions[:, 1] < max_diam) * (positions[:, 0] > (box_size - max_diam)))[0]
    idxs_topleft = np.nonzero(
        (positions[:, 1] > (box_size - max_diam)) * (positions[:, 0] < max_diam))[0]
    idxs_topright = np.nonzero((positions[:, :] > (box_size - max_diam)))[0]

    extra_idxs = [idxs_left,       idxs_right,  idxs_bottom,      idxs_top,
                  idxs_botleft, idxs_botright, idxs_topleft, idxs_topright]

    shifts = _get_shifts(box_size)
    return extra_idxs, shifts


def _get_shifts(box_size):
    return box_size * np.array([[1, 0], [-1, 0], [0, 1],  [0, -1],
                                [1, 1], [-1, 1], [1, -1], [-1, -1]])
